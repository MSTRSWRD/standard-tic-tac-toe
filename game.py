def print_board(entries):
    line = "+---+---+---+"
    output = line
    n = 0
    for entry in entries:
        if n % 3 == 0:
            output = output + "\n| "
        else:
            output = output + " | "
        output = output + str(entry)
        if n % 3 == 2:
            output = output + " |\n"
            output = output + line
        n = n + 1
    print(output)
    print()

def game_over(board, current_player):
    print_board(board)
    print("GAME OVER")
    print(board[current_player], "has won")
    exit()

def is_row_winner(board, row_number):
    # 0 1 1 2, 3 4 4 5, 6 7 7 8
    if board[row_number - 1] == board[row_number] and board[row_number] == board[row_number + 1]:
        print("Winner row: ", row_number)
        return True

def is_column_winner(board, column_number):
    # 0 3 3 6, 1 4 4 7, 2 5 5 8
    if board[column_number - 3] == board[column_number] and board[column_number] == board[column_number + 3]:
        print ("Winner column:", column_number)
        return True

def is_diagonal_winner(board, diagonal_number):
    # 0 4 4 8, 2 4 4 6
    if board[diagonal_number - 4] == board[diagonal_number] and board[diagonal_number] == board[diagonal_number + 4]:
        print ("Winner diagonal: ", diagonal_number)
        return True
    elif board[diagonal_number - 2] == board[diagonal_number] and board[diagonal_number] == board[diagonal_number + 2]:
        print ("Winner diagonal: ", diagonal_number)
        return True


board = [1, 2, 3, 4, 5, 6, 7, 8, 9]
current_player = "X"

for move_number in range(1, 10):
    print_board(board)
    response = input("Where would " + current_player + " like to move? ")
    space_number = int(response) - 1
    board[space_number] = current_player


    if is_row_winner(board, 1):
        game_over(board, space_number)
    elif is_row_winner(board, 4):
        game_over(board, space_number)
    elif is_row_winner(board, 7):
        game_over(board, space_number)
    elif is_column_winner(board, 3):
        game_over(board, space_number)
    elif is_column_winner(board, 4):
        game_over(board, space_number)
    elif is_column_winner(board, 5):
        game_over(board, space_number)
    elif is_diagonal_winner(board, 4):
        game_over(board, space_number)

    if current_player == "X":
        current_player = "O"
    else:
        current_player = "X"

print("It's a tie!")
